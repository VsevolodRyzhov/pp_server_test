﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System;

namespace PP_api
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex is HttpException)
            {
                Server.ClearError();
                Response.Clear();
                Response.TrySkipIisCustomErrors = true;
                Response.AddHeader("content-type", "application/json");
                switch (((HttpException)ex).GetHttpCode()) {
                    case 404:
                        Response.StatusCode = 404;
                        Response.Write("{\"Error\":{\"Code\":404,\"Status\":\"Invalid Endpoint\"}}");
                        break;
                    case 400:
                        Response.StatusCode = 400;
                        Response.Write("{\"Error\":{\"Code\":400,\"Status\":\"Bad Request\"}}");
                        break;
                    case 405:
                        Response.StatusCode = 405;
                        Response.Write("{\"Error\":{\"Code\":400,\"Status\":\"Method Not Allowed\"}}");
                        break;
                    default:
                        Response.StatusCode = 404;
                        Response.Write("{\"Error\":{\"Code\":404,\"Status\":\"Invalid Endpoint\"}}");
                        break;
                }
            }
        }
    }
}
