﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PP_api
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "not_found",
                url: "not_found",
                defaults: new { controller = "OrderWebApi", action = "NotFound" }
            );

            routes.MapRoute(
                name: "orders_search",
                url: "orders_search",
                defaults: new { controller = "OrderWebApi", action = "OrdersSearch", count = UrlParameter.Optional, date_from = UrlParameter.Optional, date_to = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "orders",
                url: "orders/{id}",
                defaults: new { controller = "OrderWebApi", action = "Order" }
            );
        }
    }
}
