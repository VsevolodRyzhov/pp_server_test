﻿using System.Web.Http;

namespace PP_api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            /*
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "orders_search",
                routeTemplate: "orders_search/{count}",
                defaults: new { controller = "OrderWebApi", action = "OrdersSearch" }
            );

            config.Routes.MapHttpRoute(
                name: "orders",
                routeTemplate: "orders/{id}",
                defaults: new { controller = "OrderWebApi", action = "Order" }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"));*/
        }
    }
}
