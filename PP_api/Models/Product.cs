﻿using System;
namespace PP_api.Models
{
    public class Product
    {
        public string barcode;
        public string price;
        public string cost;
        public string tax_perc;
        public string tax_amt;
        public string quantity;
        public string tracking_number;
        public string canceled;
        public string shipped_status_sku;

        public Product()
        {
        }
    }
}
