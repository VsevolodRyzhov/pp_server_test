﻿using System;
using System.Collections.Generic;

namespace PP_api.Models
{
    public class Order
    {
        public string orderId;
        public string phone;
        public string shipping_status;
        public string shipping_price;
        public string shipping_payment_status;
        public string payment_status;

        public List<Product> orderItems;

        public Order()
        {
            this.orderItems = new List<Product> { };
        }
    }
}
