﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PP_api.Controllers
{
    public class OrderWebApiController : Controller
    {
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return Json(new { });
        }

        public static List<Models.Order> GetTestData()
        {
            var product_1 = new Models.Product { barcode = "4062345021658", canceled = "N", cost = "24300", price = "24300", quantity = "1", shipped_status_sku = "not sent", tax_amt = "4050", tax_perc = "20", tracking_number = "1Z05V36Y7951053268" };
            var product_2 = new Models.Product { barcode = "4062345067851", canceled = "N", cost = "37800", price = "37800", quantity = "1", shipped_status_sku = "sent", tax_amt = "4050", tax_perc = "20", tracking_number = "1Z05V36Y7951053268" };
            var order_1 = new Models.Order { orderId = "PP0400104913", phone = "+79620230303", shipping_status = "not sent", shipping_price = "1000", shipping_payment_status = "paid", payment_status = "paid" };
            var order_2 = new Models.Order { orderId = "PP040023123", phone = "+79620230303", shipping_status = "not sent", shipping_price = "1000", shipping_payment_status = "not paid", payment_status = "not paid" };
            order_1.orderItems.Add(product_1);
            order_1.orderItems.Add(product_2);
            order_2.orderItems.Add(product_1);
            return new List<Models.Order>()
            {
                order_1,
                order_2
            };
        }

        [HttpPost]
        public ActionResult OrdersSearch(String count, String date_from, String date_to)
        {
            return Json(OrderWebApiController.GetTestData(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Order(String id)
        {
            return Json(new Models.Order { orderId = "PP040023123", phone = "+79620230303", shipping_status = "not sent", shipping_price = "1000", shipping_payment_status = "not paid", payment_status = "not paid" }, JsonRequestBehavior.AllowGet);
        }
    }
}
